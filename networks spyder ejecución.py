# -*- coding: utf-8 -*-
"""
Created on Sat Apr 13 19:40:13 2019

@author: yomis
"""
##### Crear red
N = 3000
kav = 10
a = 1
b = 3
net, adj = randomn(N,kav)
adj2 = weighted_net(N,adj,a,b)

####### 
it = 300
beta = 0.00001
alpha = 0.03
nH, nZ, nR, suma, est1 = SIR_model(N, adj2, beta, alpha, it)

####### Gráficos
plt.plot( np.linspace(0,it,it), nH, color = 'blue')
plt.plot( np.linspace(0,it,it), nZ, color = 'red')
plt.plot( np.linspace(0,it,it), nR, color = 'green')

adj3 = adjacency_survivors(adj2,est1,N)
pesos_finales = adj3.data[adj3.data.nonzero()]
np.average(adj3.data[adj3.data.nonzero()])


def media_pesos_MA ( matriz_de_adjacencia ):
    sum = 0
    cont = 0
    matriz = matriz_de_adjacencia.todense()
    for i in range(0,len(matriz)):
        for j in range (0,len(matriz)):
            if adj2[i,j] != 0:
                print(adj2[i,j] , "i: ", i ,"j: ", j)
                cont+=1
                sum += adj2[i,j]
    return sum / cont

def media_pesos_MAdense ( matriz):
    sum = 0
    cont = 0
    #matriz = matriz_de_adjacencia.todense()
    for i in range(0,len(matriz)):
        for j in range (0,len(matriz)):
            if adj2[i,j] != 0:
                print(adj2[i,j] , "i: ", i ,"j: ", j)
                cont+=1
                sum += adj2[i,j]
    return sum / cont