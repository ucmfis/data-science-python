# -*- coding: utf-8 -*-
"""
Created on Sat Apr 13 19:37:59 2019

@author: yomis
"""
import networkx as nx 
import numpy as np
import matplotlib.pyplot as plt
import random as r
import scipy.sparse as scp
from matplotlib.ticker import PercentFormatter
from scipy import sparse



###############################################################################
def randomn(N, kav):
    p = kav/(N -1)
    net = nx.fast_gnp_random_graph(N, p, directed = False) 
    #net = nx.barabasi_albert_graph(N, kav) 
    adj = nx.adjacency_matrix(net) # adj is a Sparse Matrix of CSR (compressed sparse row) format. 
                               # this means that row slicing is fast
    return net, adj

def weighted_net(N,adj,a,b):
    adj2 = adj.copy()
    adj2 = scp.csr_matrix.astype(adj2, float)   #Cambia la matriz de adjacencia a tipo float
    for i in range(0,N):
        links = adj2[i,i+1:].indices
        for j in range(0, len(links)):
            peso = r.uniform(0.01, 1)
            #peso = np.random.beta(a,b)
            adj2[i, links[j] + i + 1] = peso
            adj2[links[j] + i + 1,i] = peso
    return adj2

def probren1(prob, n, peso):
    return prob*(peso*(n**2 - 1)/n + 1/n)

def probren2(prob, peso):
    return prob + peso*(1- prob)

def probren3(prob , n , peso):
    if peso > 0.8:
        return 1
    else:
        return 0

def remove_deadz(net,est1,N):
    # Eliminamos los nodos con estado = 2 (removed)
    net2 = net.copy()
    for i in range(0,N):
        if est1[i] == 2:
            net2.remove_node(i)
    return net2

def remove_deadzh(net,est1,N):
    # Eliminamos los nodos con estado = 2 (removed)
    net2 = net.copy()
    for i in range(0,N):
        if est1[i] == 2 or est1[i] == 3:
            net2.remove_node(i)
    return net2

def adjacency_survivorsLegacy(adj2, est1, N):
    # Hacemos una nueva matriz de adyacencia en la que eliminar los links de los nodos removed
    adj3 = adj2.copy()
    #print(adj3)
    adj3 = adj3.todense()
    for i in range(0,N):
        if est1[i] == 2 or est1[i] == 3: #si el nodo está removed, hay que eliminar sus links
            links = np.where(adj3[i,:] !=0. )[1]
            for l in links:
                adj3[i,l] = 0.
                adj3[l,i] = 0.
    return adj3

def adjacency_survivors(adj2, est1, N):
    # Hacemos una nueva matriz de adyacencia en la que eliminar los links de los nodos removed
    adj3 = adj2.copy()
    #print(adj3)
    #adj3 = adj3.todense()
    cont = 0
    for i in range(0,N):
        cont +=1
        if est1[i] == 2 or est1[i] == 3: #si el nodo está removed, hay que eliminar sus links
            print(adj3[i])
            print(cont)
            for link in range(0,np.size(adj3[i])+1):
                adj3[i,link]=0
                adj3[link,i]=0
            print(adj3[i])
    return adj3


def weights_survivor(adj3, N):
    # recuperamos los pesos de cada nodo
    pesos = []
    for i in range(0,N):
        for j in range(i+1,N):
            if adj3[i,j] != 0.:
                pesos.append(adj3[i,j])
                print(adj3[i,j])
    return pesos

#########################################################################################
    
def SIR_model(N, adj2, beta, alpha, it):
    # "est" es un vector que guarda el estado de cada nodo de la red
    #  est = 0 humano; est = 1 zombie; est = 2 zombie muerto

    est = np.zeros(N, dtype = int)
    est1 = np.zeros(N, dtype = int)

    # paciente zero: el primer infectado por el virus
    pac0 = r.randint(0, N-1)
    est[pac0] = 1
    est1[pac0] = 1

    nH = np.zeros(it, dtype = int)
    nZ = np.zeros(it, dtype = int)
    nR = np.zeros(it, dtype = int)
    suma = np.zeros(it, dtype = int)

    for t in range(0, it): # time

        ##### RECUENTO #####
        nH[t] = len(np.where(est1 == 0)[0])
        nZ[t] = len(np.where(est1 == 1)[0])
        nR[t] = len(np.where(est1 == 2)[0])

        ##### INFECTION #####
        # Identificamos los zombies en la red y las infecciones que producen

        zombies = np.where(est == 1)[0]

        for i in range(0, len(zombies)): # tenemos que buscar los contactos con humanos de cada zombie

            z = zombies[i] # nodo z es un zombie
            rel = adj2[z,:].indices # nodos que están conectados con z
            relh = rel[est[rel]==0] # nodos humanos conectados con z

            for j in range(0, len(relh)): # para cada nodo humano conectado con un zombie, comprobamos si está infectado
                betaren = probren3(beta, 10, adj2[z,relh[j]])
                if r.uniform(0, 1) < betaren: # the node is infected
                    est1[relh[j]] = 1 # human turns into zombie
                    print(adj2[z,relh[j]])


        ##### ZOMBIE REMOVAL #####
        # En cada paso temporal, eliminamos a ciertos zombies que no han sido infectados en esta misma iteración.

        zombies_removal = np.where(est == 1)[0] # buscamos los zombies que NO han aparecido en esta misma iteración
        for i in range(0, len(zombies_removal)):
            if r.uniform(0, 1) < alpha:
                est1[zombies_removal[i]] = 2 

        est = est1
        suma[t] = nH[t] + nZ[t] + nR[t]
        
    return nH, nZ, nR, suma, est1
